import React from "react"
import "./layout.css"
import { GlobalStyle } from "../styles/GloablStyles"
import Header from "./header"

function Layout({ children }) {
  return (
    <>
      <GlobalStyle />
      <Header />
      <main>{children}</main>
    </>
  )
}

export default Layout
