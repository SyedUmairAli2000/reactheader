module.exports = {
  siteMetadata: {
    title: `React header component`,
    description: `A design foucsed react header component`,
    author: `@SyedUmairAli2000`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/static/images`,
      },
    },
    `gatsby-plugin-styled-components`,
  ],
}
